class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :calculator

  def initialize
    @calculator = []
  end

  def push(num)
    @calculator << num.to_f
  end

  def value
    @calculator.last
  end

  def plus
    perform_operation(:+)
  end

  def times
    perform_operation(:*)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end


  def tokens(string)
    tokens = string.split(" ")
    ops = "*-/+"
    tokens.map! { |el| ops.include?(el) ? el.to_sym : el.to_i }

    tokens
  end


  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    self.value
  end


  private

  def perform_operation(operator)
    raise "calculator is empty" if @calculator.size < 2
    second = @calculator.pop
    first = @calculator.pop
    case operator
    when :+
      @calculator << first + second
    when :-
      @calculator << first - second
    when :*
      @calculator << first * second
    when :/
      @calculator << first.fdiv(second)
    else
      @calculator << first
      @calculator << second

      raise "Error"


    end
  end


end
